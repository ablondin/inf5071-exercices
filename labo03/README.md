# Laboratoire 3: Modélisation

## 1 - Interface de Blender (30 mins)

Familiarisez avec l'interface de base du logiciel Blender, notamment avec les
éléments suivants:

* Modifier la vue principale à l'aide de la souris (*zoom*, *panning*, rotation)
* Ajouter (`SHIFT-A`) et supprimer (`X`) des objets dans une scène
* Passer en mode *Object* ou *Edit* (`TAB`) et modifier un maillage par sommet,
  arête ou face
* Les différentes façons de sélectionner des éléments (*all* = `A`, *box*
  = `B`, *circle* = `C`, en maintenant `SHIFT` pour accumuler, etc.)
* Les transformations de base (rotation, changement d'échelle, translation)
  autant en mode *Object* que *Edit*
* Le menu de gauche (*tools*) qu'on peut montrer/cacher avec `T`
* Le menu de droite (*properties*) qu'on peut montrer/cacher avec `N`

Si vous êtes à la maison, les tutoriels pour débutants de [BlenderGuru (Andrew
Price)](https://www.youtube.com/watch?v=JYj6e-72RDs) sont particulièrement bien
faits, mais prendront plus de 30 minutes à visionner.

## 2 - Modélisation d'une chaise (90 mins)

**Note:** Pour cet exercice ne soyez pas trop minutieux et limitez-vous à 60
minutes.

### 2.1 - Décomposition de l'image en deux images

* Récupérez l'image ci-bas disponible dans le répertoire courant
* À l'aide de la commande `convert` (et de ses options `-size` et `-extract`),
  procédez à l'extraction de la vue de face et de la vue de côté
* Nommez vos fichiers respectivement `chair-front.png` et `chair-side.png`

<table>
  <tr><td><img src="chair-blueprints.png" alt="Projections d'une chaise"/></td></tr>
  <tr><td>Source: <a href="https://www.the-blueprints.com/blueprints/misc/furniture/21211/view/lake_placid_high_back_chair/">the-blueprints.com</a></td></tr>
</table>

### 2.2 - Affichage des images de référence

* Lancez Blender et passez en vue orthogonale (touche `5`)
* Chargez ensuite les deux images pour chacune des deux vues (section
  *Background Images* dans l'onglet de droite, qui peut être activé/désactivé
  avec la touche `N`)

### 2.3 - Modélisation d'une chaise

* En démarrant avec un plan ou un cube, modélisez la chaise en utilisant la
  transformation non destructrice (*modifier*) *Mirror*
* Utilisez également les trois transformations de base afin de bien aligner
  votre maillage avec les images de référence: rotations (`R`), translations
  (*grab*, `G`) et changements d'échelle (*scaling*, `S`)
* Prenez soin d'alterner entre les vues de face et de côté pendant la
  modélisation
* Ne soyez pas trop perfectionniste si l'alignement n'est pas idéal, les images
  de références ne sont là que pour vous aider à positionner les différents
  éléments, mais vous n'avez pas à les respecter parfaitement
* Lorsque vous avez terminé, ajoutez un matériau de base (couleur simple)
  à votre chaise

**Note:** Il y a plusieurs façons de modéliser la chaise. Dans mon cas, j'ai
utilisé les stratégies suivantes:

* J'agissais dans la plupart des cas sur les faces, mais parfois aussi sur les
  arêtes
* J'ai fait plusieurs *extrusions* (`E`)
* J'ai fait quelques *coupures* (*loop cut*, `CTRL-R`) lorsque je voulais créer
  une petite face carrée au milieu d'un morceau afin de faire une extrusion
  ensuite
* Les opérations de base de rotations (`R`), de translations (`G`) et de
  changements d'échelle (`S`) pour ajuster
* Lorsque je devais joindre des faces, j'utilisais `CTRL-E` suivi de *Bridge
  edge loops*
* Et lorsque j'avais des arêtes superflues, je les supprimais avec l'option de
  dissolution (`X` suivi de *Dissolve edges*)

### 2.4 - Faire un rendu du modèle

* Faites un rendu de votre modèle en modifiant la position de la caméra et la
  taille de votre image
* Sauvegardez l'image une fois qu'elle vous satisfait

Dans mon cas, voici un exemple de rendu:

![Rendu d'une chaise](chair-render.png)

### 2.5 - Exportation et importation

* Dans Blender, exportez votre modèle aux formats OBJ, Collada et FBX
* Ensuite, démarrez Godot ou Unity et créez un projet 3D
* Importez le modèle de chaise que vous avez créé en utilisant le format approprié
* Si plusieurs formats sont supportés par votre moteur de jeu, comparez la
  différence
