# Laboratoire 7: Matériaux et lumière

## 1 - Champs de vecteurs et carte de normales (40 minutes)

L'objectif de cette question est de compléter le script Python
[`normales.py`](./normales.py) disponible dans ce dépôt afin de générer une
carte de normales à partir d'un champ de vecteurs $f(x,y)$.

### 1.1 - Familiarisation avec le script

Dans un premier temps, consultez le script et familiarisez-vous avec son
contenu. Identifiez les deux fonctions à compléter.

### 1.2 - Compléter la fonction `color`

Ensuite, complétez la fonction `color` de la classe `Vec3D`

```py
    def color(self, max_value=256):
        r"""
        Returns the color representation of a vector.
        
        More precisely, returns triplet of values between 0 and `max_value`
        describing the RGB values.

        NOTE: assumes that the vector is a unit vector
        """
        raise NotImplemented, 'A completer'
```

Cette fonction prend un vecteur unitaire $`\vec{v}`$ et le transforme en
triplet $`(r, g, b)`$, où $`0 \leq r, g, b < max\_value`$.

**Rappel**: comme les composantes d'un vecteur unitaire sont dans l'intervalle
$`[-1,1]`$, il faut ajouter 1 à chacune des composantes pour se ramener dans
l'intervalle $`[0,2]`$ puis ensuite se ramener dans l'intervalle
$`[0,max\_value]`$.

### 1.3 - Compléter la fonction `make_ppm_normal_map`

Finalement, complétez la fonction `make_ppm_normal_map`:

```py
def make_ppm_normal_map(width, height, f, xmin, xmax, ymin, ymax):
    r"""
    Prints a PPM string to stdout of the normal map.
    """
    print(HEADER % (width, height))
    raise NotImplemented, 'A completer'
```

Il suffit d'écrire sur la sortie standard un fichier au format
[PPM](https://en.wikipedia.org/wiki/Netpbm_format). Ce format est simplement un
format texte dans lequel on écrit chacun des triplets RGB au format texte.

**Remarque:** Il y a plusieurs façon de procéder. Si `f` est une fonction de
deux variables (par exemple `f = lambda x,y: sin(x)*cos(y)`), vous pouvez
construire un vecteur normal au point $`(x, y, f(x,y))`$ en approximant la
dérivée par rapport à $`x`$ en prenant les vecteurs

```python
u = (x + xtep, 0, f(x + xstep, y)) - (x - xstep, 0, f(x - xstep, y))
v = (0, y + ytep, f(x, y + ystep)) - (0, y - ystep, f(x, y - ystep))
```

puis en calculant leur produit vectoriel.


### 1.4 - Valider votre programme

Vous pouvez visualiser votre image en la convertissant en PNG avec `convert`:

```sh
convert <(python3 normales.py) image.png
```

Vous devriez obtenir le résultat suivant:

![Carte de normales](normales.png)

## 2 - Lumière (40 minutes)

Considérez une scène composée des éléments suivants:

* Le plan infini d'équation cartésienne $`z = 0`$
* Une sphère de rayon $`1`$ centrée en $`(0,0,1)`$
* Une lumière ponctuelle située en $`(0,0,3)`$

### 2.1 - Modélisation de la scène

Représentez la scène décrite ci-haut en Blender.

### 2.2 - Géométrie

Répondez aux questions suivantes:

* Quelle est la forme de la région du plan qui est ombrée (c'est-à-dire non
  éclairée par la lumière ponctuelle)?
* Décrivez de façon mathématique la région ombrée
* Quelle est la forme de la région du plan qui est ombrée si on déplace la
  lumière en position $`(3,0,3)`$?
* Proposez une position pour la lumière qui fait en sorte que la région ombrée
  soit non bornée
* Quelle serait la forme de l'ombre si on remplaçait la sphère par un tore et
  qu'on conservait la lumière en position $`(0,0,3)`$?

## 3 - Correction du devoir 1 (30 minutes)

Consultez la correction du devoir 1 [disponible ici](./devoir1-solution.pdf).
