from math import sin, cos, sqrt

EPSILON = 0.000001
PI = 3.141592653
HEADER = r"""P3
%s %s
255"""

class Vec3D(object):

    def __init__(self, x, y, z):
        r"""
        Initializes a 3D vector.
        r"""
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.
        r"""
        return 'Vec3d(%s,%s,%s)' % (self.x, self.y, self.z)

    def cross(self, other):
        r"""
        Returns the cross product of self with other.

        NOTE: the result is also a vector
        r"""
        return Vec3D(self.y * other.z - self.z * other.y,\
                     self.z * other.x - self.x * other.z,\
                     self.x * other.y - self.y * other.x)

    def square_norm(self):
        r"""
        Returns the square of the norm of self.
        """
        return self.x * self.x + self.y * self.y + self.z * self.z

    def norm(self):
        r"""
        Returns the norm of self.
        """
        return sqrt(self.square_norm())

    def normalize(self):
        r"""
        Normalizes self.

        In other words, modifies the vector such that the direction is
        preserved, but the norm becomes 1.

        NOTE: assumes that the vector is non null or almost null
        """
        n = self.norm()
        assert n >= EPSILON
        self.x /= n
        self.y /= n
        self.z /= n

    def color(self, max_value=256):
        r"""
        Returns the color representation of a vector.
        
        More precisely, returns triplet of values between 0 and `max_value`
        describing the RGB values.

        NOTE: assumes that the vector is a unit vector
        """
        raise NotImplemented, 'A completer'

def make_ppm_normal_map(width, height, f, xmin, xmax, ymin, ymax):
    r"""
    Prints a PPM string to stdout of the normal map.
    """
    print(HEADER % (width, height))
    raise NotImplemented, 'A completer'

make_ppm_normal_map(width=256, height=256, f=lambda x, y: sin(x)*cos(y),\
        xmin=0.0, xmax=2.0*PI, ymin=0.0, ymax=2.0*PI)
