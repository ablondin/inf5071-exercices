from math import sqrt

INFINITY = float("inf")
STYLE = 'scale=0.2, wall/.style={line width=1mm}, '\
        'point/.style={circle, fill=black, minimum size=1.5mm, inner sep=0pt}, '\
        'fleche/.style={draw, -{Latex[length=2mm]}, dashed, red}, '\
        'disk/.style={draw=blue, very thick, fill=blue!30}'
EPSILON = 0.0001

# Utils

def quadratic_roots(a, b, c):
    r"""
    Returns the roots of the equation `ax^2 + bx + c = 0`.
    """
    d = b * b - 4 * a * c
    if d < 0:
        return []
    elif d == 0:
        return [-b / (2.0 * a)]
    else:
        return [(-b + sqrt(d)) / (2.0 * a), (-b - sqrt(d)) / (2.0 * a)]

# Vector

class Vector(object):

    def __init__(self, x, y):
        r"""
        Returns a 2D vector.
        """
        self.x = x
        self.y = y

    def __repr__(self):
        r"""
        Returns a string representation of self.
        """
        return 'Vec2D(%s, %s)' % (self.x, self.y)

    def square_norm(self):
        r"""
        Returns the square of the norm of self.
        """
        return self.x * self.x + self.y * self.y
    
    def norm(self):
        r"""
        Returns the norm of self.
        """
        return sqrt(self.square_norm())
    
    def dot_product(self, other):
        r"""
        Returns the dot product between self and other.
        """
        return self.x * other.x + self.y * other.y

    def __add__(self, other):
        r"""
        Adds self with other (usual vector addition).
        """
        return Vector(self.x + other.x, self.y + other.y)

    def __neg__(self):
        r"""
        Returns the additive opposite of self.
        """
        return Vector(-self.x, -self.y)

    def __sub__(self, other):
        r"""
        Subtracts other from self (usual vector difference).
        """
        return Vector(self.x - other.x, self.y - other.y)

    def __rmul__(self, scalar):
        r"""
        Multiply self by the given scalar.
        """
        return Vector(self.x * scalar, self.y * scalar)

    def reflect(self, normal):
        r"""
        Returns the vector obtained from self by reflecting it with respect to
        `normal`.
        """
        return self - 2 * self.dot_product(normal) / normal.dot_product(normal) * normal

# Disk

class Disk(object):

    def __init__(self, center, radius):
        r"""
        Returns a 2D disk of given `center` and `radius`.
        """
        self.center = center
        self.radius = radius

    def ray_intersection(self, position, direction):
        r"""
        Returns the information relative to the collision of the ray with self.

        If there is no collision, then `t = inf`.

        INPUT:

        - `position`: the current position of the ray
        - `direction`: the current direction of the ray

        OUTPUT:

        A triple `(t, p, d)` where
        
        - `p` is the position of the collision
        - `t` is the "time" taken by the ray to travel from `position` to `p`
        - `d` is the reflected direction
        """
        # TODO
        pass

# Scene

class Scene(object):

    def __init__(self, width, height):
        r"""
        Returns a scene instance.

        INPUT:

        - `width`: the width of the scene
        - `height`: the height of the scene
        """
        self.width = width
        self.height = height
        self.rays = []
        self.disks = []

    def add_disk(self, disk):
        r"""
        Adds a disk to the scene.
        """
        self.disks.append(disk)

    def wall_intersection(self, position, direction):
        r"""
        Returns the position and direction of the ray after rebound.

        More precisely, starting from `position` in `direction`, finds the
        closest point on a wall where the ray collides and returns the
        colliding position together with the new direction of the ray

        INPUT:

        - `position`: the current position of the ray
        - `direction`: the current direction of the ray

        OUTPUT:

        A triple `(t, p, d)` where
        
        - `p` is the position of the collision
        - `t` is the "time" taken by the ray to travel from `position` to `p`
        - `d` is the reflected direction
        """
        # TODO
        pass

    def cast_ray(self, position, direction, num_bounds):
        r"""
        Adds the trajectory of a ray in the scene.

        INPUT:

        - `position`: the initial position of the ray
        - `direction`: the initial direction of the ray
        - `num_bounds`: the number of bounds
        """
        # TODO
        pass

    def to_tikz(self):
        r"""
        Returns a TikZ string of self.

        More precisely, the scene is drawn as follows:

        - A rectangle for the walls
        - A sequence of dots and arrows for the ray trajectory
        """
        s = '\\begin{tikzpicture}[%s]\n' % STYLE
        s += '  \draw[wall] (0,0) rectangle (%smm, %smm);\n' %\
                (self.width, self.height)
        s += '  \\node[below left]  at (0,0) {$(0,0)$};\n'
        s += '  \\node[above left] at (0,%smm) {$(0,%s)$};\n' %\
                (self.height, self.height)
        s += '  \\node[below right] at (%smm,0) {$(%s,0)$};\n' %\
                (self.width, self.width)
        s += '  \\node[above right] at (%smm,%smm) {$(%s,%s)$};\n' %\
                (self.width, self.height, self.width, self.height)

        # TODO

        s += '\\end{tikzpicture}'
        return s

scene = Scene(500, 300)
scene.add_disk(Disk(Vector(350, 100), 30))
scene.add_disk(Disk(Vector(200, 250), 40))
scene.cast_ray(Vector(100, 100), Vector(7.0, 1.7), 7)
print(scene.to_tikz())
