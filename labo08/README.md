# Laboratoire 8: Lancer de rayon

L'objectif de ce laboratoire est d'écrire un programme qui permet de simuler la
trajectoire d'un rayon de lumière dans une scène 2D.

À la fin, vous devriez être en mesure de générer une image comme celle ci-bas:

![Lancer de rayon dans une scène 2D](./rayon.png)

## 1 - Récupérer le programme Python (10 mins)

Dans un premier temps, récupérez le programme [`rayon.py`](./rayon.py)
disponible dans ce répertoire. Comme il est assez long de programmer un lancer
de rayon, une partie du travail a déjà été faite. Votre objectif est donc de
compléter le programme, en étudiant d'abord son contenu.

Noter que pour simplifier le travail, l'image est produire au format TikZ. Vous
trouverez également dans ce répertoire un script [`tikz2pdf`](./tikz2pdf) qui
vous permet de convertir le fichier texte au format TikZ en image PDF. Par
exemple, vous pouvez transforme le fichier [`rayon.tikz`](./rayon.tikz)
à l'aide de la commande

```sh
./tikz2pdf rayon.tikz -o rayon.pdf
```

ce qui produira l'image [`rayon.pdf`](./rayon.pdf).

## 2 - Scène sans obstacle (40 minutes)

Pour le moment, lorsque vous entrez les commandes

```sh
python3 rayon.py > vide.tikz
./tikz2pdf vide.tikz -o vide.pdf
```

vous obtenez une scène vide comme ci-bas:

![Scène vide](./vide.png)

Modifiez le programme [`rayon.py`](./rayon.py) qui simule la trajectoire d'un
rayon dans la scène rectangulaire. Vous devriez obtenir un résultat semblable
à celui-ci:

![Rayon dans une scène vide](./juste_rayon.png)

## 3 - Ajout des disques dans la scène (20 mins)

Modifiez le programme [`rayon.py`](./rayon.py) afin d'afficher les disques dans
la scène. Pour le moment, laissez les rayons passer à travers. Vous devriez
obtenir le résultat suivant:

![Rayon avec disques](./avec_disque.png)

## 4 - Ajout des disques (40 mins)

Finalement, ajoutez la réflexion des rayons par rapport aux disques pour
obtenir l'image présentée au début du laboratoire.

*Indice*: Utilisez la solution de la question 2 du devoir 1 qui indique comment
calculer l'intersection d'une droite et d'un cercle.
