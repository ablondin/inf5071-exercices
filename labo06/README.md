# Laboratoire 6: Animations

## 1 - Tore (30 mins)

Dans Blender, concevez une animation d'une petite sphère qui tourne autour d'un
tore, comme illustré ci-bas:

![Une sphère tournant autour d'un tore](tore.gif)

Vous devez respecter les spécifications suivantes:

* Le tore doit avoir un grand rayon de 1 unité et un petit rayon de 0.25 unités
  (les dimensions par défaut dans Blender)
* L'animation doit s'étendre sur 100 images (*frames*)
* La sphère doit faire 1 seul grand tour complet pendant qu'elle fait 5 petits
  tours
* La sphère doit se déplacer à vitesse constante

### Suggestions

* Pour la trajectoire autour du petit cercle, je vous recommande d'utiliser une
  courbe de Bézier circulaire en combinaison avec une contrainte de type
  *Follow Path*.
* Pour faire tourner le long du grand cercle, vous pouvez simplement utiliser
  un objet vide (*Empty*) comme parent de la courbe de Bézier circulaire et
  animer sa rotation.
* Dans l'éditeur de graphes (*Graph editor*), il est possible d'extrapoler une
  courbe de façon cyclique en choisissant *Channel*, *Extrapolation Mode*,
  *Make cyclic*.

## 2 - Bras mécanique (60 mins)

L'objectif de cette question est de vous familiariser avec un bras mécanique
déjà modélisé, articulé, habillé et animé.

Voici un rendu du modèle:

![Rendu 3D d'un bras mécanique](mechanical-arm-render.png)

Ainsi qu'un GIF montrant l'animation actuelle:

![Animation d'un bras mécanique](mechanical-arm.gif)

Le modèle articulé et animé est disponible dans le fichier [Bras mécanique
animé](./mechanical-arm-animation.blend)

### 2.1 - Familiarisation avec l'objet

Dans un premier temps, familiarisez-vous avec les différents aspects du modèle:

* L'armature qui permet de l'animer (`Armature`), avec ses différents os;
* Le modèle lui-même (`MechanicalArm`);
* La cannette (`Can`);
* L'objet vide facilitant la mise en place de contrainte (`CanLocation`);
* Les différentes composantes animées sur la ligne du temps, la feuille
  d'animation (*Dopesheet*), etc.

Portez une attention particulière à la hiérarchie, ainsi qu'aux contraintes qui
lient les objets.

### 2.2 - Complétion de l'animation

Complétez l'animation pour que le bras mécanique revienne à sa position de
départ après avoir déposé la cannette. L'animation devrait s'étendre sur 120
images (*frames*). Vous devriez obtenir le résultat suivant:

![Animation souhaitée](mechanical-arm-result.gif)

**Note**: Il est possible de copier la pose initiale du bras mécanique et de la
coller ensuite à la dernière image. Il suffit pour cela, lorsque vous êtes en
mode *Pose*, de cliquer sur l'option *Pose* -> *Copy Pose*.

## 3 - Placage d'une texture (30 mins)

Considérez l'image suivante:

![Texture d'un dé](./dice.png)

Dans Blender, appliquez la texture ci-haut sur un cube de telle sorte que la
somme de chaque paire de faces opposées donne 7. Vous devriez obtenir un modèle
semblable à celui-ci:

![Rendu d'un dé](dice-render.png)
