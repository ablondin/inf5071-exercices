# Laboratoire 5: Paramétrisation

## 1 - Courbes paramétrées (40 mins)

Dans cette question, on souhaite dessiner des courbes paramétrées en 2D et en
3D à l'aide du logiciel [Gnuplot](http://www.gnuplot.info/).

### 1.1 - Ellipse

L'ellipse de centre $`(h,k)`$ et de rayons $`a`$, $`b`$ peut être paramétrée
par la fonction vectorielle

```math
\vec{r}(t) = (h,k) + (a\cos t, b\sin t)
```

Complétez le script Gnuplot suivant en remplaçant `x(t)` et `y(t)` par la bonne
expression afin de dessiner l'ellipse de centre `(1,2)` et de rayons `3` et
`2`:

```gnuplot
#!/usr/bin/gnuplot
set terminal pdf
set output "ellipse.pdf"
set parametric
set trange [0:2*pi]
set samples 10000
set nokey
plot x(t),y(t)
```

### 1.2 - Hélice

Considérez maintenant l'hélice en 3D dont la paramétrisation est

```math
\vec{r}(t) = (\cos t, \sin t, t)
```

* Montrez que l'hélice est complètement incluse sur le cylindre d'équation
  cartésienne $`x^2 + y^2 = 1`$
* À l'aide de Gnuplot, tracez l'hélice en remplaçant `x(u)`, `y(u)` et `z(u)`
  par les expressions appropriées dans le script suivant:

```gnuplot
#!/usr/bin/gnuplot
set terminal pdf
set output "helix.pdf"
set parametric
set urange [-4*pi:4*pi]
set samples 1000
set nokey
splot x(u),y(u),z(u)
```

### 1.3 - Spirale

Une [spirale archimédienne](https://en.wikipedia.org/wiki/Archimedean_spiral)
est une courbe 2D dont l'équation en coordonnées polaires est

```math
r = a + b\theta
```

où $`a`$ et $`b`$ sont des nombres réels positifs. De la même façon qu'aux
sous-questions précédentes, créez un script Gnuplot qui permet de dessiner une
spirale archimédienne en prenant $`a = 0`$ et $`b = 0.5`$.

**Remarque**. Dans un premier temps, vous devez transformer l'équation polaire
en paramétrisation $`\vec{r}(t)`$.

## 2 - Surfaces paramétrées (40 mins)

### 2.1 - Disque

Écrivez un script Gnuplot qui permet de dessiner le disque de centre
$`(0,0,1)`$ et de rayon $`1`$ situé dans le plan $`z = 0`$. Vous devriez
obtenir le résultat suivant:

![Un disque dessiné avec Gnuplot](./disk.png)

### 2.3 - Tube

Écrivez un script en Blender qui génère un tube de hauteur $`h`$, de rayon
intérieur $`r1`$, de rayon extérieur $`r2`$, centré à l'origine et ayant $`n`$
subdivisions verticales. Par exemple, le tube ci-bas est obtenu en prenant $`h
= 2`$, $`r1 = 0.8`$, $`r2 = 1.0`$ et $`n = 32`$:

![Un tube dans Blender](./tube-viewport.png)

*Aide*. Vous pouvez démarrer avec le script [`tube.py`](./tube.py) disponible
dans ce répertoire.

*Remarque*. Il est assez facile de positionner les sommets, qui peuvent être
divisés en 4 groupes, selon qu'ils se trouvent sur un cercle intérieur ou
extérieur, en haut ou en bas. Ce qui est un peu plus difficile, c'est de
décrire les faces en respectant l'ordre des numéros des sommets. N'hésitez pas
à introduire une fonction supplémentaire qui vous aidera à simplifier cette
correspondance.

## 3 - Animation (40 mins)

Concevez une animation dans Blender d'un petit cube qui se déplace à vitesse
constante le long d'un cercle, en tournant sur lui-même, aussi à vitesse
constante. On s'attend à la fin à obtenir un résultat similaire à celui-ci:

![Cube animé le long d'un cercle](./animation.gif)

### 3.1 - Création des objets

* Lancez une nouvelle scène dans Blender
* Dans un premier temps, redimensionnez le cube pour qu'il soit assez petit
  (par exemple en appliquant un changement d'échelle de 0.1)
* Ensuite, ajoutez un cercle (dans *curve* et non dans *mesh*) à l'origine

### 3.2 - Configuration

* Changez les paramètres de la ligne du temps pour que l'animation s'étende sur
  80 images (*frames*)
* Modifiez également les dimensions du rendu à 800 pixels par 500 pixels, avec
  une qualité de 100%
* Après avoir coché l'option *Lock view to camera*, positionnez la caméra pour
  obtenir un angle de vue intéressant
* Effectuez toute autre configuration pertinente (couleur d'arrière-plan,
  matériaux des objets, format de sortie, etc.)

### 3.2 - Animation du cube

* Assurez-vous que le cube est positionné à l'origine
* À l'image 1, fixez l'angle de rotation autour de l'axe z à $`0^\circ`$
* À l'image 81, fixez l'angle de rotation autour de l'axe z à $`720^\circ`$
* Ensuite, rendez-vous dans l'éditeur de graphe (*graph editor*)
* Appuyez sur `HOME` afin d'avoir une vue d'ensemble des modifications
* Désélectionnez tout et sélectionnez tous les points de la courbe
* Choisissez *Key*, *Handle Type* et *Vector* pour rendre la vitesse de
  rotation constante
* Vérifiez que votre animation de rotation est correcte et rectifiez si
  nécessaire

### 3.3 - Animation le long d'une courbe

* En sélectionnant seulement le cube, allez dans l'onglet *Constraints*
  (l'icône de deux maillons d'une chaîne)
* Ajoutez une contrainte de type *Follow path*
* Comme cible (*target*), sélectionnez la courbe circulaire
* Ensuite, sélectionnez la courbe circulaire et allez dans l'onglet *Data*
  (l'icône représente deux sommets liés par une courbe)
* Assurez-vous que la section *Path animation* est cochée et modifiez le nombre
  d'images (*frames*) à 80
* Ensuite, à l'image 1 sur la ligne du temps, fixez la propriété *Evaluation
  time* à 0
* Puis à l'image 81 sur la ligne du temps, fixez la propriété *Evaluation time*
  à 1
* En principe, votre cube devrait maintenant se déplacer le long du cercle
* De la même façon que vous l'avez fait pour la rotation du cube, vous pouvez
  modifier la forme de la courbe du paramètre *Evaluation time* de sorte qu'il
  évolue à vitesse constante

### 3.4 - Rendu de l'animation

Faites un rendu de votre animation. Si vous avez installé le greffon
[Sprifity](https://github.com/Fweeb/blender_spritify), vous pouvez également
produire un GIF animé, comme je l'ai fait dans cette page.
