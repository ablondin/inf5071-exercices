import bpy
from math import pi, cos, sin

def cylindrical_to_cartesian(r, theta, z):
    r"""
    Returns the cartesian coordinates from the cylindrical ones.
    """
    return (r * cos(theta), r * sin(theta), z)

# Vertices and faces
n = 32
h = 2.0
r1 = 0.8
r2 = 1.0
theta_step = 2.0 * pi / n
verts = []
faces = []

# Model
mesh_data = bpy.data.meshes.new('Tube')
mesh_data.from_pydata(verts, [], faces)
mesh_data.update()
obj = bpy.data.objects.new('Tube', mesh_data)

# Scene
scene = bpy.context.scene
scene.objects.link(obj)
obj.select = True
bpy.ops.wm.save_as_mainfile(filepath='tube.blend')
