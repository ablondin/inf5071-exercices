# Laboratoire 4: Géométrie 3D

## 1 - Vecteurs normaux d'une sphère (30 mins)

Écrivez une fonction Python qui, étant donnée une sphère `s` et un point `p`,
retourne

* le vecteur normal à la sphère `s` au point `p` si `p` est un point qui se
  trouve **sur** la sphère `s`
* le vecteur nul si `p` se trouve à l'**intérieur** ou à l'**extérieur** de la
  sphère `s`

**Remarque:** Il est probable que vous ayez des problèmes d'arrondis étant
donné que vous utiliserez la fonction `sqrt`. Par conséquent, plutôt que de
tester l'égalité de deux valeurs flottantes, vous devrez plutôt vérifier si
leur différence est proche de zéro.

## 2 - Génération des sommets d'un cône (40 mins)

Écrivez une fonction Python qui affiche au format OBJ les sommets d'un cône de
centre $`C`$, de hauteur $`h`$, de rayon $`r`$ (le rayon de sa base) et dont la
base a $`n`$ sommets.

Par exemple, prenons le [cône par défaut en Blender](./cone.blend). Ses
paramètres sont $`C = (0.0, 0.0)`$, $`h = 2`$, $`r = 1`$ et $`n = 32`$. Si on
exporte le cône au format OBJ, on obtient le [fichier suivant](./cone.obj) dont
les sommets sont:

```
v 0.000000 -1.000000 -1.000000
v 0.195090 -1.000000 -0.980785
v 0.382683 -1.000000 -0.923880
v 0.555570 -1.000000 -0.831470
v 0.707107 -1.000000 -0.707107
v 0.831470 -1.000000 -0.555570
v 0.923880 -1.000000 -0.382683
v 0.980785 -1.000000 -0.195090
v 1.000000 -1.000000 -0.000000
v 0.980785 -1.000000 0.195090
v 0.923880 -1.000000 0.382683
v 0.831470 -1.000000 0.555570
v 0.707107 -1.000000 0.707107
v 0.555570 -1.000000 0.831470
v 0.382683 -1.000000 0.923880
v 0.195090 -1.000000 0.980785
v -0.000000 -1.000000 1.000000
v -0.195091 -1.000000 0.980785
v -0.382684 -1.000000 0.923879
v -0.555571 -1.000000 0.831469
v -0.707107 -1.000000 0.707106
v -0.831470 -1.000000 0.555570
v -0.923880 -1.000000 0.382683
v 0.000000 1.000000 0.000000
v -0.980785 -1.000000 0.195089
v -1.000000 -1.000000 -0.000001
v -0.980785 -1.000000 -0.195091
v -0.923879 -1.000000 -0.382684
v -0.831469 -1.000000 -0.555571
v -0.707106 -1.000000 -0.707108
v -0.555569 -1.000000 -0.831470
v -0.382682 -1.000000 -0.923880
v -0.195089 -1.000000 -0.980786
```

Votre programme Python devrait donc produire le résultat ci-haut, en tenant
compte des aspects suivants:

* Lorsqu'on exporte de Blender au format OBJ, les axes $`y`$ et $`z`$ sont
  inversés.
* L'ordre d'affichage de vos sommets peut être différent de celui qui apparaît
  plus haut. L'important est de retrouver les mêmes coordonnées au moins une
  fois.
* Il se peut qu'il y ait des différences de valeurs aux dernières décimales
  selon votre façon d'arrondir les nombres, qui peut être différente de celle
  utilisée par Blender.

## 3 - Courbes (30 mins)

### 3.1 - Construction de courbes

Dans Blender, familiarisez-vous avez les différentes courbes (*curves*)
disponibles (Bezier, NURBS, etc.):

* Étudiez comment modifier la forme de ces courbes en mode édition (*edit*)
* Construisez des chemins avec un nombre arbitraire de points de contrôle. Plus
  précisément, utilisez l'opérateur d'extrusion (`E`) à plusieurs reprises sur
  une extrémité de la courbe afin de la prolonger en un nouveau point

### 3.2 - Déformation d'un objet

Construisez un maillage simple que vous aimeriez déformer le long d'une courbe.
Par exemple, partez d'un cube, appliquez-lui un changement d'échelle par
rapport à l'axe $`x`$, et utilisez la coupure (`CTRL-R`) afin de le subdiviser.
Plus la subdivision sera importante, plus la déformation sera précise

Ensuite, utilisez la modification non destructrice (*modifier*) nommée *Curve*
sur l'objet que vous avez créé, puis déformez-le en sélectionnant une des
courbes que vous avez manipulées plus tôt.

Vous devriez obtenir un résultat similaire à celui de l'image ci-bas:

![Déformation d'un modèle selon une courbe](curve-modifier.png)
