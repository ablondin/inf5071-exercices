# Laboratoire 1: Création et manipulation d'images

## 1 - Présentation (10 mins)

* Faites connaissance avec votre démonstrateur (Éric Lavallée).
* Connectez-vous aux machines de l'UQAM avec vos accès UQAM.
* *Remarque*: c'est une bonne idée d'apporter votre propre machine aux
  laboratoires pour une plus grande flexibilité d'installation de logiciels.
* Lisez l'énoncé complet du laboratoire avant de commencer

## 2 - Krita (40 mins)

### 2.1 - Installation

Installez le logiciel [Krita](https://krita.org/en/) sur votre machine.
Ensuite, lancez l'application et familiarisez-vous superficiellement avec son
interface.

### 2.2 - Création d'une texture périodique

L'objectif de cette sous-question est de créer une texture périodique de
dimensions $`1024 \times 1024`$ similaire à celle-ci (encore une fois, le côté
artistique n'est pas important):

![Une texture périodique de pierres](stones.png)

* Créez d'abord un nouveau document en choisissant les dimensions $`1024 \times
  1024`$.
* À l'aide de la brosse de votre choix, dessinez votre texture. Notez que ça se
  fait bien avec la souris (pas besoin d'avoir une tablette graphique) vu que
  la qualité n'est pas importante.
* Utilisez au moins deux couches (*layers*): une couche nommée *Background* qui
  contient l'arrière-plan d'une couleur plus foncée et une autre nommée
  *Stones* (ou *Bricks* si vous préférez des briques).
* Je vous rappelle qu'une texture est périodique si on ne perçoit pas à l'oeil
  de rupture lorsqu'on place 4 copies de l'image en haut, en bas, à gauche et
  à droite.
* *Aide*: Dans Krita, la touche `w` permet d'activer/désactiver la vue
  périodique.

### 2.3 - Sauvegarde et exportation

* Sauvegardez votre projet au format `kra`.
* Ensuite, exportez l'image au format `png` dans le même répertoire.
* Dans un terminal, affichez les métadonnées de l'image `png` et du fichier
  `kra` à l'aide de la commande `file`.
* Ouvrez l'image `png` à l'aide du visualisateur d'image par défaut de votre
  système.

## 3 - Inkscape (40 mins)

### 3.1 - Installation

Installez le logiciel [Inkscape](https://inkscape.org/) sur votre machine.
Ensuite, lancez l'application et familiarisez-vous superficiellement avec son
interface.

### 3.2 - Création d'une image vectorielle

L'objectif de cette sous-question est de créer une petite image vectorielle
représentant un bouton « Play » similaire à celle-ci (notez que le côté
artistique n'est pas important):

![Un exemple de bouton « Play »](play.png)

* Modifiez d'abord la taille du canevas (dans *File -> Document Properties*)
  pour que votre image soit de taille $`256 \times 256`$.
* Ensuite, créez un disque. En Inkscape, la touche `Ctrl` permet de conserver
  le ratio pour avoir un cercle et non une ellipse. Assurez-vous de contrôler
  sa couleur de remplissage (*fill*) et de contour (*stroke*).
* Centrez horizontalement le disque dans le canevas (voir *Object -> Align and
  Distribute*).
* Ajoutez une boîte de texte contenant le mot « Play » ou « Jouer ». Prenez
  aussi soin de la centrer horizontalement.
* Finalement, ajoutez le triangle à l'aide de l'icône pour tracer des courbes
  de Bézier et des segments de droite. Assurez-vous d'obtenir une courbe
  *fermée* (point de départ = point d'arrivée). Inkscape l'indique à l'aide
  d'un petit carré rouge.
* Comme pour le disque, modifiez les couleurs de remplissage et de contour.

### 3.3 - Sauvegarde et exportation

* Sauvegardez votre projet sous le nom `play.svg`.
* Ensuite, exportez l'image au format `png` sous le nom `play.png` dans le même
  répertoire.
* Dans un terminal, affichez les métadonnées de l'image `png` et du fichier
  `svg` à l'aide de la commande `file`.
* Ouvrez l'image `png` à l'aide du visualisateur d'image par défaut de votre
  système.
* Ouvrez le fichier `svg` à l'aide d'un éditeur de texte. Examinez la structure
  de son contenu. Portez une attention particulière aux zones qui encodent le
  disque, le triangle et la boîte de texte.

## 4 - ImageMagick (30 mins)

[ImageMagick](https://www.imagemagick.org/) est un logiciel qui permet de
manipuler facilement des images. Il offre en particulier les commandes
`convert` et `montage`

### 4.1 - Rogner une image

* Récupérez l'image `play.png` produite à l'exercice précédent.
* En ligne de commande, utilisez `convert` pour la rogner.
* *Aide*: l'option `-trim` devrait vous être utile. Prenez le temps de bien
  consulter le manuel (`man convert`) pour connaître la syntaxe à utiliser.
* Quelles sont les dimensions de l'image résultante?

### 4.2 - Redimensionner une image

* Toujours avec l'image `play.png`, redimensionnez-la à l'aide de `convert` et
  de son option `-resize`.
* Qu'en est-il de la qualité de l'image résultante? Qu'observez-vous quand vous
  l'affichez?
* Répétez le redimensionnement en ajoutant l'option `-quality`.
* Comparez ensuite le résultat selon que vous utilisez l'option `-resize`,
  `-adaptive-resize` et `-interpolative-resize`

### 4.3 - Combiner des images

* Récupérez la texture périodique créée un peu plus tôt. Supposons que le
  fichier se nomme `stones.png`.
* Produisez un petit pavage $`2 \times 2`$ à l'aide de la commande `montage`
  pour vérifier que votre texture est bien périodique. Vous devriez obtenir une
  image ressemblant à celle ci-bas.
* *Aide*: consultez les options `+clone`, `-geometry` et `-tile`. En
  particulier, il est possible de supprimer l'espacement entre les tuiles avec
  l'option `-geometry +0+0`.

![Pavage 2x2 d'une texture périodique](tiling.png)
