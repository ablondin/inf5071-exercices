# Infographie

Ce dépôt contient les énoncés des laboratoires du cours INF5071 Infographie,
enseigné à l'UQAM.

Vous n'avez qu'à cliquer sur le lien correspondant pour accéder à l'énoncé. Il
peut être aussi une bonne idée de cloner le dépôt afin de récupérer certains
fichiers qui pourraient être fournis en complément.

* [Labo 1](labo01/README.md)
* [Labo 2](labo02/README.md)
* [Labo 3](labo03/README.md)
* [Labo 4](labo04/README.md)
* [Labo 5](labo05/README.md)
* [Labo 6](labo06/README.md)
* [Labo 7](labo07/README.md)
* [Labo 8](labo08/README.md)
* [Labo 9](labo09/README.md)
