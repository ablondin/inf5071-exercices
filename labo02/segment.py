class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return 'Point(%s,%s)' % (self.x, self.y)

class Segment:
    
    def __init__(self, point0, point1):
        self.point0 = point0
        self.point1 = point1
        assert self.point0 != self.point1

    def __repr__(self):
        return 'Segment between %s and %s' % (self.point0, self.point1)

    def contains(self, point):
        # A completer
        # Voir diapos du chapitre 02-images
        return True

# Tests

p0s = [Point(x,y) for (x,y) in\
        [(1.0, 3.0), (0.0,4.0), (-2.0,0.0), (1.0,-1.0)]]
p1s = [Point(x,y) for (x,y) in\
        [(5.0,-5.0), (8.0,0.0), ( 2.0,0.0), (1.0,-4.0)]]
ps  = [Point(x,y) for (x,y) in\
        [(3.0, 6.0), (4.0,2.0), ( 3.0,0.0), (1.0,-2.0)]]

for (p0, p1, p) in zip(p0s, p1s, ps):
    s = Segment(p0, p1)
    print('s = %s' % s)
    print('p = %s' % p)
    print('p on s? %s' % ('yes' if s.contains(p) else "no"))
