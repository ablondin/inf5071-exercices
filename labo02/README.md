# Laboratoire 2: Génération d'images

## 1 - Dessiner une horloge avec TikZ (30 mins)

Considérez les deux fichiers suivants disponibles dans le répertoire courant:

* un script shell [`draw_clock`](./draw_clock)
* un fichier LaTeX [`template.tex`](./template.tex)

L'objectif de cette question est de modifier le fichier `template.tex` de telle
sorte que lorsqu'on entre la commande

```sh
./draw_clock 4 28
```

alors on obtient une image `tmp/clock.pdf` représentant une horloge qui affiche
4 heures 28 minutes, comme dans l'image ci-bas:

![L'horloge qu'on souhaite obtenir](clock-result.png)

Or, actuellement, si vous entrez la commande, il manque les traits secondaires
qui identifient les minutes et l'horloge indique toujours la même heure sans
tenir compte des arguments, comme suit:

![L'horloge générée actuellement](clock-current.png)

### 1.1 - Coordonnées polaires

Familiarisez-vous d'abord avec le script et le gabarit fournis. En particulier,
en TikZ, il est possible de désigner des coordonnées polaires avec la syntaxe
`(angle:rayon)` (le séparateur est le caractère `:`), alors que les coordonnées
cartésiennes sont de la forme `(x,y)` (le séparateur est le caractère `,`).

Une fois que vous avez bien compris comment fonctionnent les deux fichiers
individuellement ainsi que leur interaction, complétez les deux parties
manquantes décrites dans la suite.

### 1.2 - Position des aiguilles

Le positionnement correct des aiguilles se fait en modifiant les valeurs des
variables `\am` et `\ah` qui dénotent respectivement les angles des minutes et
des heures.

```tex
\am = 30;
\ah = 180;
```

Il suffit donc d'exprimer `\am` et `\ah` en fonction des valeurs de `\h` et de
`\m` et des opérations arithmétiques de base (`+`, `-`, `*` et `/`). Notez que
l'angle de l'aiguille des minutes ne dépend que de `\m`, mais que l'angle de
l'aiguille des heures dépend autant de `\m` que de `\h`

### 1.3 - Ajout des traits secondaires

Vous pouvez ajouter les traits secondaires (des minutes) en utilisant une
boucle de la forme

```tex
\foreach \b in { ... } {
  [...]
}
```

que vous imbriquez dans la boucle

```tex
\foreach \a [count=\h] in {30,60,...,360} {
  [...]
}
```

## 2 - Manipulation d'images (30 mins)

L'objectif de cette question est de manipuler des images à l'aide de outil
[ImageMagick](https://www.imagemagick.org/).

### 2.1 - Convertir une image de couleur en niveaux de gris

Récupérez le logo de Krita ci-bas:

![Logo de Krita](./krita-logo.png)

À l'aide de la commande `convert`, transformez l'image en niveau de gris de
quatre façons différentes:

* En utilisant l'option `-grayscale`
* En remplaçant chaque pixel par la *moyenne* des canaux rouge, vert et bleu,
  par exemple à l'aide de l'option `-fx`
* En remplaçant chaque pixel par le *maximum* des canaux rouge, vert et bleu
* En remplaçant chaque pixel par le *minimum* des canaux rouge, vert et bleu

Est-ce que toutes les images sont différentes? Quels résultats vous semblent
les plus intéressants?

### 2.2 - Transformations géométriques

Toujours avec le logo de Krita, appliquez les transformations géométriques
suivantes à l'aide d'une seule ligne de commande dans chacun des cas en
utilisant l'outil `convert`:

* Rotation de 45 degrés
* Rotation de 90 degrés
* Rotation de 180 degrés
* Réflexion par rapport à un axe horizontal
* Réflexion par rapport à un axe vertical
* Réflexion par rapport à un axe diagonal
* Changement d'échelle de 50%
* Changement d'échelle de 200%

## 3 - Point et segment (30 mins)

Considérez le module Python [`segment.py`](./segment.py) disponible dans le
répertoire courant. Complétez la fonction `contains` de la classe `Segment` de
telle sorte qu'elle retourne vrai si et seulement si `point` se trouve sur le
segment courant.

Inspirez-vous du raisonnement vu en classe dans la section *Géométrie 2D* du
chapitre *02-images*.

## 4 - Génération d'une texture avec Pillow (30 mins)

La bibliothèque [Pillow](https://pillow.readthedocs.io/en/latest/index.html)
est une bibliothèque Python qui permet de manipuler des images.

En utilisant cette bibliothèque, générez de façon procédurale une texture de
briques de dimensions $`256 \times 256`$ similaire à celle présentée ci-bas.

![Une texture de briques](briques.png)

Il est recommandé de consulter la page de référence sur le module
[ImageDraw](https://pillow.readthedocs.io/en/latest/reference/ImageDraw.html)
pour vous familiariser avec les fonctions disponibles.

Voici les paramètres que j'ai utilisés pour représenter le mur, afin qu'il soit
plus facile d'obtenir une texture périodique (*seamless*) :

- `xmargin` : la demi-longueur horizontale entre deux briques;
- `ymargin` : la demi-longueur verticale entre deux briques;
- `xwidth` : la largeur horizontale entre le début de deux briques (donc la
  largeur d'une brique est `xwidth - xmargin`);
- `ywidth` : la hauteur entre le début de deux briques (donc la hauteur d'une
  brique est `ywidth - ymargin`.
