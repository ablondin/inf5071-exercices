# Laboratoire 9: Shaders 2D

L'objectif de ce laboratoire est de créer un *shader* 2D représentant un
hexagone qui tourne autour du centre de l'écran, comme dans le jeu [Super
Hexagon](https://www.youtube.com/watch?v=5mDjFdetU28), créé par [Terry
Cavanagh](https://terrycavanaghgames.com/). À la fin, vous devriez obtenir un
résultat similaire à celui-ci :

![](hexagon.webm)

Afin de simplifier l'installation et la compilation, vous allez travailler sur
le site [shadertoy.com](https://www.shadertoy.com/), qui permet de produire
facilement un *shader*.

Voici les fonctions qui sont utilisées dans ma solution. Je vous encourage
à consulter leur documentation :

- [`normalize`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/normalize.xhtml),
  qui permet de normaliser un vecteur;
- [`length`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/length.xhtml),
  qui retourne la longueur d'un vecteur;
- [`dot`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/dot.xhtml),
  qui retourne le produit scalaire de deux vecteurs;
- [`step`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/step.xhtml),
  qui permet de « binariser » entre `0.0` et `1.0` selon qu'une valeur atteint
  un seuil;
- [`smoothstep`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/smoothstep.xhtml),
  une version avec anticrénelage de `step`;
- [`cos`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/cos.xhtml),
  [`sin`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/sin.xhtml),
  [`atan`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/atan.xhtml),
  des fonctions trigonométriques très utiles !
- [`mod`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/mod.xhtml),
  la fonction modulo habituelle, qui marche sur des nombres réels, pas juste
  sur des entiers !
- [`max`](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/max.xhtml),
  qui calcule le maximum de deux vecteurs, composante par composante.

**Remarque 1** : Je me suis mis comme contrainte de n'utiliser aucune structure
de contrôle (conditionnelle et boucle), seulement des fonctions. Je vous
encourage à faire de même afin de vous pratiquer à utiliser les fonctions
mathématiques de GLSL.

**Remarque 2 :** Bien qu'une démarche soit suggérée pour compléter l'exercice,
si vous préférez, n'hésitez pas à utiliser votre propre démarche !

## 1 - Distance entre un point et une droite

À partir de la [page
Wikipédia](https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line)
correspondante, implémentez une fonction `distPointLine` qui calcule la
distance entre un point `p` et une droite passant par des points `p1` et `p2`,
dont l'en-tête est la suivante :

```c
// Returns the distance between p and the line going through p1 and p2
float distPointLine(vec2 p, vec2 p1, vec2 p2) {
    // TODO
    return 0.;
}
```

## 2 - Point d'un hexagone

Écrivez une fonction nommée `point` qui retourne le $`i`$-ième point d'un
hexagone régulier, centré en `c`, se trouvant sur le cercle de rayon `r`, et
ayant subi une rotation d'angle `a`, numéroté de $`0`$ à $`5`$, dont l'en-tête
est la suivante :

```c
// Returns the i-th point of the hexagon of radius r, centered at c,  of angle a
vec2 point(float a, vec2 c, float r, int i) {
    // TODO
    return vec2(0., 0.);
}
```

**Remarque :** L'angle `a` sera utilisé plus tard pour animer la rotation de
l'hexagone.

## 3 - Point sur un segment

Écrivez une fonction nommée `onSegment` qui retourne un nombre entre `0.0` et
`1.0` indiquant si le point `p` se trouve sur le segment qui relie les points
`p1` et `p2`. Son en-tête est la suivante:

```c
// Indicates if p lies on the segment between p1 and p2
float onSegment(vec2 p, vec2 p1, vec2 p2) {
    // TODO
    return 0.;
}
```

**Remarque 1:** Vous devrez faire appel à la fonction `distPointLine`.

**Remarque 2:** Dans la version sans anticrénelage, vous pouvez retourner `0.0`
si le point ne se trouve pas sur le segment et `1.0` s'il s'y trouve (la
fonction `step` vous sera utile). Si vous souhaitez intégrer de l'anticrénelage
(pour avoir un plus beau résultat), vous pouvez retourner un nombre entre `0.0`
et `1.0` (la fonction `smoothstep` vous sera utile).

## 4 - Point sur un segment

Combinez les fonctions précédentes afin de tracer l'hexagone à l'écran. Plus
précisément, complétez la fonction suivante, qui indique si `p` se trouve sur
l'hexagone de rayon `r`, centré en `c` et ayant subi une rotation d'angle `a` :

```c
// Indicates if p lies on the hexagon of radius r centered at c, rotated by angle a
float onHexagon(vec2 p, vec2 c, float r, float a) {
    // TODO
    return 0.;
}
```

## 5 - Dessin des zones

Dessiner les zones (les trois « triangles » infinis) déterminées par
l'hexagones. Pour cela, il suffit de compléter la fonction suivante :

```c
// Indicates if p lies in the regions induced by the hexagon centered in c, rotated by angle a
float inZone(vec2 p, vec2 c, float a) {
    vec2 u = p - c;
    float b = mod(atan(u.y, u.x) - a, 2. * PI / 3.);
    return step(PI / 3., b);
}
```

## 6 - Animation de l'hexagone

Animez les paramètres de votre choix à l'aide de la variable globale `iTime`.
Par exemple, vous pouvez jouer avec les paramètres suivants :

- La rotation de l'hexagone et des zones. Dans le jeu, elle tourne à vitesse
  constante pendant un certain temps, puis ensuite change subitement de
  vitesse. Vous pouvez vous amuser avec ça.
- La couleur de l'hexagone. Dans le jeu original, la couleur alterne entre les
  3 couleurs primaires (rouge, bleu, vert) et les 3 couleurs secondaires (cyan,
  jaune, magenta).
- Le rayon de l'hexagone. Dans le jeu, celui-ci varie selon le rythme de la
  musique.
